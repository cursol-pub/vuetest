import $ from "jquery";
import _ from "lodash";

export default class IuCallCard {
  cardInitializer = () => {};
  foldedCardInitializer = () => {};
  bxCallCardRoot = null;
  appRoot = null;

  /**
   * Initialize vue app at
   */
  initApplication() {
    if (!this.appRoot) return;
    this.cardInitializer('[data-ref="iu-call-card"]');
  }

  /**
   *  Initialize call card
   */
  initFoldedCard() {
    console.log("initializing folded card");
    this.foldedCardInitializer('[data-ref="iu-call-card-folded"]');
  }

  /**
   * At the bitrix call card construct process
   * partially replace its html by ours
   */
  decorateBxCallCard(
    cardInitializer = () => {},
    foldedCardInitializer = () => {}
  ) {
    this.cardInitializer = cardInitializer;
    this.foldedCardInitializer = foldedCardInitializer;
    this.log("Decorating call card");
    try {
      const self = this;

      let showCardAllowed = false;

      // decorate `show` card method to prevent non-crm card view
      const showOriginal = window.BX.PhoneCallView.prototype.show;
      window.BX.PhoneCallView.prototype.show = function () {
        if (!showCardAllowed) return;
        showOriginal.call(this);
        showCardAllowed = false;
      };

      const createLayoutCrmOriginal =
        window.BX.PhoneCallView.prototype.createLayoutCrm;
      window.BX.PhoneCallView.prototype.createLayoutCrm = function () {
        const originalRes = createLayoutCrmOriginal.call(this);

        showCardAllowed = true;
        this.show();

        self.bxCallCardRoot = $(originalRes);
        self.appRoot = self.bxCallCardRoot.find(".im-phone-call-container");
        self.appRoot.attr("data-ref", "iu-call-card");
        self.appRoot.empty();

        (async () => {
          await self.appInsertedInDom();
          self.initApplication();
        })();

        return originalRes;
      };

      const createLayoutFoldedOriginal =
        window.BX.PhoneCallView.prototype.createLayoutFolded;
      window.BX.PhoneCallView.prototype.createLayoutFolded = function () {
        const originalRes = createLayoutFoldedOriginal.call(this);

        const bxCallCardFoldedRoot = $(originalRes);
        bxCallCardFoldedRoot.attr("data-ref", "iu-call-card-folded");
        bxCallCardFoldedRoot.empty();

        (async () => {
          await self.foldedCardInsertedInDom();
          self.initFoldedCard();
        })();

        return originalRes;
      };
      this.processActiveFoldedPhoneCall();
      this.removeCallCardDoubleClickBehaviour();
      this.log(`Call card decorated`);
    } catch (e) {
      this.log(`Error while decorating call card`);
    }
  }

  /**
   * Removes bitrix default call card double click
   */
  removeCallCardDoubleClickBehaviour() {
    window.BX.PhoneCallView.prototype._onDblClick = function () {};
  }

  /**
   * Case when active call is folded and page was reloaded
   */
  processActiveFoldedPhoneCall() {
    $(
      '.im-phone-call-panel-mini:not([data-ref="iu-call-card-folded"])'
    ).remove();
    if (_.invoke(window, "BXIM.webrtc.phoneCallView.isFolded"))
      _.invoke(window, "BXIM.webrtc.phoneCallView.reinit");
  }

  /**
   * Waits until app node will be inserted in bx call card
   * @returns {Promise<void>}
   */
  async appInsertedInDom() {
    return new Promise((resolve) => {
      let mounted = !!$('[data-ref="iu-call-card"]').length;
      if (mounted) resolve();
      else {
        let tries = 0;
        const timer = window.setInterval(() => {
          mounted = !!$('[data-ref="iu-call-card"]').length;
          if (++tries > 300) {
            clearTimeout(timer);
            resolve();
            return;
          }
          if (mounted) {
            clearTimeout(timer);
            resolve();
            return;
          }
        }, 30);
      }
    });
  }

  /**
   * Waits until folded call card appears in DOM
   * @returns {Promise<void>}
   */
  async foldedCardInsertedInDom() {
    return new Promise((resolve) => {
      let mounted = !!$('[data-ref="iu-call-card-folded"]').length;
      if (mounted) resolve();
      else {
        const timer = window.setInterval(() => {
          mounted = !!$('[data-ref="iu-call-card-folded"]').length;
          if (mounted) {
            clearTimeout(timer);
            resolve();
            return;
          }
        }, 30);
      }
    });
  }

  log(message = "") {
    console.log(
      `%c${message}`,
      "color: #4d1c62; padding-left: 20px; background-repeat: no-repeat; background-size: 15px; background-image: url(\"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' id='logo_letter' viewBox='0 0 19458 14082'%3E%3Cpath d='M6736 2153c0 889 619 1131 914 1131 189 0 377 0 539-54 834-242 1562-1157 1562-2099C9751 484 9535 0 8835 0 7758 0 6736 996 6736 2153zm11261 1750c-1597 1579-3194 3158-4792 4738-403 403-2099 1857-2771 1857-378 0-835-431-835-1454 0-269 54-511 161-780 861-565 3788-2026 3769-3823-7-496-496-669-1208-553-1012 164-3347 1988-4071 4044-285 812-385 1662-402 2560-104 75-221 170-385 275-1131 727-1831 1023-2450 1023-673 0-1615-162-1615-1750 0-457-28-753 135-1103 2368-1077 3672-2611 3654-3823-19-1385-1580-961-2681-24-1196 1017-2189 2462-2751 3470-193 216-403 418-556 549-824 705-1141 792-1186 918-36 102 3 230 130 276 153 54 624-118 1229-416-34 263-47 526-47 799 0 1831 753 3396 2234 3396 1184 0 2474-703 4410-2718 244 1230 779 2207 1495 2207 1296 0 2934-1862 4253-3584-188 753-481 1861-404 2584 118 1101 539 1511 1104 1511 1420 0 4841-3269 5033-3853 43-128-108-216-215-216-134 0-323 270-1266 808-375 216-1157 565-1533 565-512 0-700-538-700-1103 0-765 520-1753 849-2096 812-845 1623-1690 2435-2535 92-97 108-134 108-323v-242c-27-1184-404-1480-674-1480-188 0-323 162-457 296z'%3E%3C/path%3E%3C/svg%3E\");"
    );
  }
}
